console.log("Hello javascript objects");

// Array - a collection of related data

const grades = [91, 92, 93, 94, 95];
const names = ["Joy", "Natalia", "Bianca"];

// Objects - a collection of multiple values with different data types.

const objGrades = {
	firstName: "Aaron",
	lastName: "Delos Santos",
	firstGrading: 91,
	subject: "English",
	teacher: ["Carlo", "Mashiro"],
	isActive: true,
	schools: {
		city: "Manila",
		country: "Philippines"
	},
	studentNames: [
	{
		name: "Nikko",
		batch: "152"
	},
	{
		name: "Adrian",
		batch: "152"
	}
	],
description: function(){
	return `${this.subject}: ${this.firstGrading} of student 
	${this.studentNames[0].name} and ${this.studentNames[1].name}`
}
}

console.log(objGrades.firstGrading); //91
console.log(objGrades.subject); //English
console.log(objGrades["teacher"]);
console.log(objGrades["firstName"]);
console.log(objGrades["lastName"]);

console.log(objGrades.description());

// In schools property, access country property
console.log(objGrades.schools.country);
console.log(objGrades["schools"]["city"]);

// In studentNames property, access the second element
console.log(objGrades["studentNames"][1]);
console.log(objGrades["studentNames"][1].name);
console.log(objGrades["studentNames"][1]["batch"]);


//Q: Is it possible to add a new property in an object?
	// A: yes, with the use of dot notation & assignment operator
objGrades.semester = "first"
console.log(objGrades);



// Q: Is it possible to delete a property in an object
	// A: Yes, 
delete objGrades.semester;
console.log(objGrades);















/* Mini Activity

Given a set of objects:

-Compute for the average grade of each student object.
-Add the computed average of the student as a value to a new property called average to the studentGrades array of objects.
-Log the modified object array (includes the average property) to the console.

Stretch Goals:
Hint: Research the use of parseFloat() and toFixed()
-Round off the average into a single decimal number.

Note: Value for average property cannot be a string.

*/

const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];



let ave1 = (studentGrades[0].Q1 + studentGrades[0].Q2 + studentGrades[0].Q3 + studentGrades[0].Q4) / 4
console.log(ave1)

studentGrades[0].average = parseFloat(ave1.toFixed(1))
console.log(studentGrades)




let ave2 = (studentGrades[1].Q1 + studentGrades[1].Q2 + studentGrades[1].Q3 + studentGrades[1].Q4) / 4
console.log(ave2)

studentGrades[1].average = parseFloat(ave2.toFixed(1))
console.log(studentGrades)




let ave3 = (studentGrades[2].Q1 + studentGrades[2].Q2 + studentGrades[2].Q3 + studentGrades[2].Q4) / 4
console.log(ave3)

studentGrades[2].average = parseFloat(ave3.toFixed(1))
console.log(studentGrades)




let ave4 = (studentGrades[3].Q1 + studentGrades[3].Q2 + studentGrades[3].Q3 + studentGrades[3].Q4) / 4
console.log(ave4)

studentGrades[3].average = parseFloat(ave4.toFixed(1))
console.log(studentGrades)




let ave5 = (studentGrades[4].Q1 + studentGrades[4].Q2 + studentGrades[4].Q3 + studentGrades[4].Q4) / 4
console.log(ave5)

studentGrades[4].average = parseFloat(ave5.toFixed(1))
console.log(studentGrades)





// SOLUTION #2 USING: FOR LOOP


for(let i = 0; i < studentGrades.length; i++){
	let ave = (studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 + studentGrades[i].Q4) / 4

	console.log(ave)
}



/*
// SOLUTION #3: USING forEach

studentGrades.forEach(function(element){
	console.log(element)

	let ave = (element.Q1 + element.Q2 + element.Q3 + element.Q4) / 4
	console.log(ave)
})

console.log(studentGrades)
*/


/* Object Constructor



*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	attack: function(){
		console.log('This Pokemon tackled targetPokemon')
		console.log('targetPokemons health is now reduced to targetPokemonhealth')
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}
console.log(myPokemon)

function Pokemon(name, lvl, hp){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attach = lvl,
	this.tackle = function(target){
		console.log(target)
		// console.log('This Pokemon tackled targetPokemon')
		// console.log(`targetPokemon's gealth is now reduced to targetPokemonhealth`)
		console.log(`${this.health} health is now reduced to ${target.health - this.attack}`)
	},
	this.faint = function(){
		console.log(`Pokemon fainted`)
	}
}

let pikachu = new Pokemon("Pikachu", 5, 50)
let charizard = new Pokemon("Charizard", 8, 40)

console.log(pikachu.tackle(charizard))

/* Mini Activity

1. Create a new set of pokemon for battle (use Pokemon object constructor)
2. Solve the health of the pokemon that when tackled, the current value of target's health must reduced conti nuously as many time as the tackle function is invoked.
3. I health of the target pokemon is now below 10, invoke the faint function. (Note: refactor faint function to display appropriate value to the statement)
*/



let object1 = {
	property: "sample"
};

let array1 = ["sample1", "sample1"];

console.log(typeof object1) ;
console.log(typeof array1);


/*
	Conventions when creating our constructor:

		To distinguish it with other functions we usually capitalize the name of our constructor.

*/







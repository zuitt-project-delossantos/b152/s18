let trainer = {
	firstName: "Ash",
	age: 30,
	pokemon: ["mew", "celebi", "rayquaza"],
	friends: ["mystie", "brock"]
}
console.log(trainer)



let talk = ("Mew! I choose you!")

console.log(talk)



function Pokemon(name, lvl, hp, attack){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target){
		console.log(target)
		console.log(`${this.health} health is now reduced to ${target.health - this.attack}`)
	},
	this.faint = function(){
		console.log(`targetPokemon fainted`)
	}
}

let mew = new Pokemon("Mew", 90, 100, 90)
let celebi = new Pokemon("Celebi", 90, 50, 90)
let rayquaza = new Pokemon("Rayquaza", 90, 100, 90)

console.log(mew.tackle(celebi))
function Pokemon(name, lvl, hp){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target){
		console.log(target)	//object of charizard

		//change the statement to "Pikachu tackled Charizard"
		console.log(`${this.name} tackled ${target.name}`)

		//chnage the statement to "Charizard's health is now reduced to #"
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)

		//Using compound assigment operator
		// current health - attack = health

		target.health
	},
	this.faint = function(){
		console.log(`Pokemon fainted`)
	}
}


// let pikachu = new Pokemon("Pikachu", 5, 50)
// let charizard = new Pokemon("Charizard", 8, 40)

// console.log(pikachu.tackle(charizard))

/* Mini Activity

1. Create a new set of pokemon for battle (use Pokemon object constructor)
2. Solve the health of the pokemon that when tackled, the current value of target's health must reduced continuously as many times as the  tackle function is invoked.
3. If health of the target pokemon is now below 10, invoke the faint function. (Note: refactor faint function to display appropriate value to the statement)

*/

let bulbasaur = new Pokemon("Bulbasaur", 12, 80)
let charmander = new Pokemon("Charmander", 20, 65)

console.log(bulbasaur)
console.log(charmander)

console.log(bulbasaur.tackle(charmander))
